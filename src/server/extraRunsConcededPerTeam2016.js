const JsonDataPromise = require("./index.js");
const fs = require('fs');

JsonDataPromise.JSONDeliveriesDataFetcher().then((data) => {

    function ExtraRunsPerTeam() {
        deliveriesIn2016 = data.filter((deliveriesObject) => {
            if (deliveriesObject.match_id >= 577 && deliveriesObject.match_id <= 636)
                return true;
            else
                return false;
        })



        let answer = deliveriesIn2016.reduce((extraRunsConcededPerTeam, deliveriesObject) => {
            if (extraRunsConcededPerTeam[deliveriesObject.bowling_team] == undefined)
                extraRunsConcededPerTeam[deliveriesObject.bowling_team] = parseInt(deliveriesObject.extra_runs);
            else
                extraRunsConcededPerTeam[deliveriesObject.bowling_team] += parseInt(deliveriesObject.extra_runs);

            return extraRunsConcededPerTeam;
        }, {});

        let jsonString = JSON.stringify(answer, null, 2);
        fs.writeFileSync("../public/output/extraRunsConcededPerTeam2016.json", jsonString);
        console.log(jsonString);
    }

    ExtraRunsPerTeam();

})