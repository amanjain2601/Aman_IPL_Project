const JsonDataPromise = require("./index.js");
const fs = require('fs');

JsonDataPromise.JSONMatchesDataFetcher().then((data) => {

   function WinTossAndWinMatch() {
      let answer = data.reduce((winTossAndWinMatch, MatchObject) => {
         if (MatchObject.toss_winner == "Rising Pune Supergiants") {
            MatchObject.toss_winner = "Rising Pune Supergiant";
            MatchObject.winner = "Rising Pune Supergiant";
         }

         if (MatchObject.toss_winner == MatchObject.winner) {
            if (winTossAndWinMatch[MatchObject.winner] == undefined)
               winTossAndWinMatch[MatchObject.winner] = 1;
            else
               winTossAndWinMatch[MatchObject.winner] += 1;
         }

         return winTossAndWinMatch;
      }, {})

      let jsonString = JSON.stringify(answer, null, 2);
      fs.writeFileSync("../public/output/winMatchAndWinTossPerTeam.json", jsonString);
      console.log(jsonString);

   }
   WinTossAndWinMatch();
})