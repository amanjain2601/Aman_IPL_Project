const csvFilePath1 = '../data/matches.csv';
const csvFilePath2 = '../data/deliveries.csv';
const csv = require('csvtojson')


function JSONMatchesDataFetcher() {
	return csv()
		.fromFile(csvFilePath1)
		.then((data) => {
			return data;
		})
}

function JSONDeliveriesDataFetcher() {
	return csv()
		.fromFile(csvFilePath2)
		.then((data) => {
			return data;
		})
}


module.exports = { JSONMatchesDataFetcher, JSONDeliveriesDataFetcher };









