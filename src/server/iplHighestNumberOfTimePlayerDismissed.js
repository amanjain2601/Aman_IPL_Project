const JsonDataPromise = require("./index.js");
const fs = require("fs");

JsonDataPromise.JSONDeliveriesDataFetcher().then((data) => {

    function HighestNumberTimesPlayerDismissed() {

        let highestNumberTimesPlayerDismissed = data.reduce((highestNumberTimesPlayerDismissed, DelieveryData) => {

            if (DelieveryData.dismissal_kind != "" && DelieveryData.dismissal_kind != "run out") {
                if (highestNumberTimesPlayerDismissed[DelieveryData.batsman] == undefined) {
                    highestNumberTimesPlayerDismissed[DelieveryData.batsman] = {};
                    highestNumberTimesPlayerDismissed[DelieveryData.batsman][DelieveryData.bowler] = 1;
                }
                else {
                    if (highestNumberTimesPlayerDismissed[DelieveryData.batsman][DelieveryData.bowler] == undefined)
                        highestNumberTimesPlayerDismissed[DelieveryData.batsman][DelieveryData.bowler] = 1;
                    else
                        highestNumberTimesPlayerDismissed[DelieveryData.batsman][DelieveryData.bowler] += 1;
                }

            }
            return highestNumberTimesPlayerDismissed;

        }, {})

        highestNumberTimesPlayerDismissed = Object.entries(highestNumberTimesPlayerDismissed);


        let highestNumberOfTimesOnePlayerDismissedByAnother = highestNumberTimesPlayerDismissed.reduce((highestNumberTimesPlayerDismissedByAnother, currentElement) => {
            currentElement[1] = Object.entries(currentElement[1]);

            currentElement[1].sort(function (a, b) {
                return b[1] - a[1];
            })

            let highestTimePlayerDismissed = currentElement[1][0][1];

            let ObjectOfPlayersWithHighestDismissedCount = currentElement[1].reduce((allBowlersWithHighestDismissedCount, currentElement) => {
                if (currentElement[1] == highestTimePlayerDismissed)
                    allBowlersWithHighestDismissedCount[currentElement[0]] = currentElement[1];

                return allBowlersWithHighestDismissedCount;
            }, {})

            highestNumberTimesPlayerDismissedByAnother[currentElement[0]] = ObjectOfPlayersWithHighestDismissedCount;

            return highestNumberTimesPlayerDismissedByAnother;
        }, {})



        let jsonString = JSON.stringify(highestNumberOfTimesOnePlayerDismissedByAnother, null, 2);
        fs.writeFileSync("../public/output/highestNumberOfTimesOnePlayerDismissedByAnother.json", jsonString);
        console.log(jsonString);

    }

    HighestNumberTimesPlayerDismissed();
})