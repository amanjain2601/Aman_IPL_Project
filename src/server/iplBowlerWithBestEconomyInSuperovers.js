const JsonDataPromise = require("./index.js");
const fs = require('fs');

JsonDataPromise.JSONDeliveriesDataFetcher().then((data) => {

    //Filter all data in deleveries file which are superovers using filter
    function BowlerWithBestEconomySuperovers() {
        let superOverDelieveries = data.filter(function (element) {
            if (element.is_super_over == 1)
                return true;
            else
                return false;
        })

        //now storing data for each bowler who has bowl a superover=>the run conceded and over balled

        let previousBowler = superOverDelieveries[0].bowler;//to store who bowl previous over and set its default value
        let NoOfBallsInOverPreviously = 0;//to store the count of balls in previous over. 
        let countExtraball = 0;//to store count of extra balls to be excluded from previous over.




        let bowlerData = superOverDelieveries.reduce((bowlerData, currentDelivery) => {
            let bowlerName = currentDelivery.bowler;
            let ballNumber = currentDelivery.ball;
            //Byes runs are excludedits not bowler fault.
            let runs = parseInt(currentDelivery.total_runs) - (parseInt(currentDelivery.legbye_runs) + parseInt(currentDelivery.bye_runs));
            if (bowlerData[bowlerName] == undefined) {
                bowlerData[bowlerName] = [0, 0];
            }

            if (ballNumber == 1) { //this if block signify a neww over has been started so reset values and store old value named with previous in data
                bowlerData[previousBowler][1] += (NoOfBallsInOverPreviously - countExtraball) / 6;

                NoOfBallsInOverPreviously = 0;
                previousBowler = bowlerName;
                countExtraball = 0;

            }

            bowlerData[bowlerName][0] += runs;
            NoOfBallsInOverPreviously = parseInt(currentDelivery.ball);

            //over do not include wide,noball and penalty
            let isWideball = parseInt(currentDelivery.wide_runs);
            let isNoball = parseInt(currentDelivery.noball_runs);
            let isPenaltyball = parseInt(currentDelivery.penalty_runs);

            if (isWideball != 0 || isNoball != 0 || isPenaltyball != 0)
                countExtraball++;

            return bowlerData;

        }, {})

        //to update bowler over he delivered value which is not covered by reduce.
        bowlerData[previousBowler][1] += (NoOfBallsInOverPreviously - countExtraball) / 6;

        bowlerData=Object.entries(bowlerData);
      

        //Now update bowler data by economy rate formulae(Runs conceded/overs bowled)

        bowlerData=bowlerData.map((element)=>{
            let bowler=[];
            bowler.push(element[0]);
            bowler.push((element[1][0]/element[1][1]).toFixed(2));

            return bowler;
        })

        //sort bowler in ascending order by their economyrate
  
        bowlerData.sort(function (a, b) {
            return a[1] - b[1];
        });

        //fetch bowler with low run rate 
        let TopBowlerWithBestEconomyData = bowlerData[0];
      
        let TopBowlerWithBestEconomy = {};
        TopBowlerWithBestEconomy[TopBowlerWithBestEconomyData[0]] = TopBowlerWithBestEconomyData[1];

        let jsonString = JSON.stringify(TopBowlerWithBestEconomy, null, 2);
        fs.writeFileSync("../public/output/topBowlerWithBestEconomyInSuperover.json", jsonString);
        console.log(jsonString);

    }

    BowlerWithBestEconomySuperovers();
});

