const JsonDataPromise = require("./index.js");
const fs = require('fs');

JsonDataPromise.JSONMatchesDataFetcher().then((data) => {

    function PlayerOfMatchForEachSeason() {
        let playerOfMatchForEachSeasonCount = data.reduce((playerOfMatchForEachSeasonCount, MatchObject) => {
            if (playerOfMatchForEachSeasonCount[MatchObject.season] == undefined) {
                playerOfMatchForEachSeasonCount[MatchObject.season] = {};
                playerOfMatchForEachSeasonCount[MatchObject.season][MatchObject.player_of_match] = 1;

            }
            else {
                if (playerOfMatchForEachSeasonCount[MatchObject.season][MatchObject.player_of_match] == undefined)
                    playerOfMatchForEachSeasonCount[MatchObject.season][MatchObject.player_of_match] = 1;
                else
                    playerOfMatchForEachSeasonCount[MatchObject.season][MatchObject.player_of_match] += 1;
            }

            return playerOfMatchForEachSeasonCount;
        }, {})

        playerOfMatchForEachSeasonCount = Object.entries(playerOfMatchForEachSeasonCount);


        let playerWithMostPlayerMatchForEachSeason = playerOfMatchForEachSeasonCount.reduce((playerWithMostPlayerMatchForEachSeason, element) => {

            element[1] = Object.entries(element[1]);
            let sortPlayersByPlayerOfMatchRecieved = element[1].map((element) => {
                let player = [];
                player.push(element[0]);
                player.push(element[1]);

                return player;
            })

            //sort players with rescpect to players of match recieved
            sortPlayersByPlayerOfMatchRecieved.sort(function (a, b) {
                return b[1] - a[1];
            });


            let mostAwards = sortPlayersByPlayerOfMatchRecieved[0][1];

            //store all the players with maximum players of match recieved
            let playerWithMostPlayerOfMatch = sortPlayersByPlayerOfMatchRecieved.reduce((playersArray, element) => {
                if (element[1] == mostAwards)
                    playersArray.push(element);

                return playersArray;
            }, [])



            playerWithMostPlayerMatchForEachSeason[element[0]] = playerWithMostPlayerOfMatch;

            return playerWithMostPlayerMatchForEachSeason;

        }, {})


        let jsonString = JSON.stringify(playerWithMostPlayerMatchForEachSeason, null, 2);
        fs.writeFileSync("../public/output/playerWithMostPlayerOfMatchEachSeason.json", jsonString);
        console.log(jsonString);

    }

    PlayerOfMatchForEachSeason();


})