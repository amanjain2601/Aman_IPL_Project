const JsonDataPromise = require("./index.js");
const fs = require('fs');

JsonDataPromise.JSONDeliveriesDataFetcher().then((data) => {

    //console.log(data);

    function Top10EconomicalBowlers() {
        let deliveriesIn2015 = data.filter((deliveryObject) => {
            if (deliveryObject.match_id >= 518 && deliveryObject.match_id <= 576)
                return true;
            else
                return false;
        })

        let previousBowler = "UT Yadav";//to store who has bowled previous over and set its default value.
        let NoOfBallsInOverPreviously = 0;//to store no of balls in previous over.
        let countExtraball = 0;//store extra balls in previous over which is bot counted.

        let top10EconomicalBowlers = deliveriesIn2015.reduce((top10EconomicalBowlers, deliveryObject) => {
            let runs = parseInt(deliveryObject.total_runs) - (parseInt(deliveryObject.legbye_runs) + parseInt(deliveryObject.bye_runs));

            let ballNumber = deliveryObject.ball;


            if (top10EconomicalBowlers[deliveryObject.bowler] == undefined)
                top10EconomicalBowlers[deliveryObject.bowler] = [0, 0];

            if (ballNumber == 1) { //this if block signify a new over has been started so reset values and store old value named with previous in data

                top10EconomicalBowlers[previousBowler][1] += (NoOfBallsInOverPreviously - countExtraball) / 6;

                NoOfBallsInOverPreviously = 0;
                previousBowler = deliveryObject.bowler;
                countExtraball = 0;

            }

            top10EconomicalBowlers[deliveryObject.bowler][0] += runs;
            NoOfBallsInOverPreviously = parseInt(deliveryObject.ball);

            //over do not include wide,noball and penalty
            let isWideball = parseInt(deliveryObject.wide_runs);
            let isNoball = parseInt(deliveryObject.noball_runs);
            let isPenaltyball = parseInt(deliveryObject.penalty_runs);

            if (isWideball != 0 || isNoball != 0 || isPenaltyball != 0)
                countExtraball++;

            return top10EconomicalBowlers;

        }, {})



        //this is used to store value of bowler not covered by reduce function
        top10EconomicalBowlers[previousBowler][1] += (NoOfBallsInOverPreviously - countExtraball) / 6;

        top10EconomicalBowlers = Object.entries(top10EconomicalBowlers);


        //divide runs by total overs which is sored as array  to get economy rate.


        //sort bowlers in ascending order with respect to economy rate

        top10EconomicalBowlers = top10EconomicalBowlers.map((element) => {
            let bowler = [];
            bowler.push(element[0]);
            bowler.push((element[1][0] / element[1][1]).toFixed(2));
            return bowler;
        })

        top10EconomicalBowlers.sort(function (a, b) {
            return a[1] - b[1];
        });

        //get top 10 bowlers finally

        top10EconomicalBowlers = top10EconomicalBowlers.reduce((BowlerWithTop10EconomyRate, currentObject, index) => {
            if (index < 10) {
                BowlerWithTop10EconomyRate[currentObject[0]] = currentObject[1];
                return BowlerWithTop10EconomyRate;
            }
            else
                return BowlerWithTop10EconomyRate;
        }, {})

        let jsonString = JSON.stringify(top10EconomicalBowlers, null, 2);
        fs.writeFileSync("../public/output/top10EconomicalBowlers.json", jsonString);
        console.log(jsonString);


    }

    Top10EconomicalBowlers();
})
