const JsonDataPromise = require("./index.js");
const fs = require('fs');


JsonDataPromise.JSONMatchesDataFetcher().then((data) => {

    function MatchesPlayedPerYear() {

        let ans = data.reduce((MatchesPlayedPerYearAnswer, MatchObject) => {
            if (MatchesPlayedPerYearAnswer[MatchObject.season] == undefined)
                MatchesPlayedPerYearAnswer[MatchObject.season] = 1;
            else
                MatchesPlayedPerYearAnswer[MatchObject.season] += 1;

            return MatchesPlayedPerYearAnswer;
        }, {});

        let jsonString = JSON.stringify(ans, null, 2);
        fs.writeFileSync("../public/output/matchesPerYear.json", jsonString);
        console.log(jsonString);
    }
    MatchesPlayedPerYear();
})
