const JsonDataPromise = require("./index.js");
const fs = require('fs');

JsonDataPromise.JSONDeliveriesDataFetcher().then((data) => {

    function StrikeRateBatsmanEachSeason() {


        let strikeRateBatsmanEachSeason = data.reduce((strikeRateBatsmanEachSeason, DelieveryData) => {

            if (DelieveryData.match_id >= 1 && DelieveryData.match_id <= 59)
                year = "2017";
            else if (DelieveryData.match_id >= 60 && DelieveryData.match_id <= 117)
                year = "2008";
            else if (DelieveryData.match_id >= 118 && DelieveryData.match_id <= 174)
                year = "2009";
            else if (DelieveryData.match_id >= 175 && DelieveryData.match_id <= 234)
                year = "2010";
            else if (DelieveryData.match_id >= 235 && DelieveryData.match_id <= 307)
                year = "2011";
            else if (DelieveryData.match_id >= 308 && DelieveryData.match_id <= 381)
                year = "2012";
            else if (DelieveryData.match_id >= 382 && DelieveryData.match_id <= 457)
                year = "2013";
            else if (DelieveryData.match_id >= 458 && DelieveryData.match_id <= 517)
                year = "2014";
            else if (DelieveryData.match_id >= 518 && DelieveryData.match_id <= 576)
                year = "2015";
            else if (DelieveryData.match_id >= 577 && DelieveryData.match_id <= 636)
                year = "2016";

            if (strikeRateBatsmanEachSeason[DelieveryData.batsman] == undefined) {
                strikeRateBatsmanEachSeason[DelieveryData.batsman] = {};
                //wide,no ball and penalty ball is not considered a delievery to batsman.
                if (parseInt(DelieveryData.wide_runs) == 0 && parseInt(DelieveryData.noball_runs) == 0 && parseInt(DelieveryData.penalty_runs) == 0)
                    strikeRateBatsmanEachSeason[DelieveryData.batsman][year] = [parseInt(DelieveryData.batsman_runs), 1];
                else
                    strikeRateBatsmanEachSeason[DelieveryData.batsman][year] = [parseInt(DelieveryData.batsman_runs), 0];
            }
            else {
                if (strikeRateBatsmanEachSeason[DelieveryData.batsman][year] == undefined) {
                    //wide,no ball and penalty ball is not considered a delievery to batsman.
                    if (parseInt(DelieveryData.wide_runs) == 0 && parseInt(DelieveryData.noball_runs) == 0 && parseInt(DelieveryData.penalty_runs) == 0)
                        strikeRateBatsmanEachSeason[DelieveryData.batsman][year] = [parseInt(DelieveryData.batsman_runs), 1];
                    else
                        strikeRateBatsmanEachSeason[DelieveryData.batsman][year] = [parseInt(DelieveryData.batsman_runs), 0];
                }
                else {
                    let previousBatsmanData = strikeRateBatsmanEachSeason[DelieveryData.batsman][year];
                    //wide,no ball and penalty ball is not considered a delievery to batsman
                    if (parseInt(DelieveryData.wide_runs) == 0 && parseInt(DelieveryData.noball_runs) == 0 && parseInt(DelieveryData.penalty_runs) == 0)
                        strikeRateBatsmanEachSeason[DelieveryData.batsman][year] = [previousBatsmanData[0] + parseInt(DelieveryData.batsman_runs), previousBatsmanData[1] + 1];
                    else
                        strikeRateBatsmanEachSeason[DelieveryData.batsman][year] = [previousBatsmanData[0] + parseInt(DelieveryData.batsman_runs), previousBatsmanData[1]];
                }
            }

            return strikeRateBatsmanEachSeason;

        }, {})



        strikeRateBatsmanEachSeason = Object.entries(strikeRateBatsmanEachSeason);

        strikeRateBatsmanEachSeason = strikeRateBatsmanEachSeason.reduce((BatsmanStrikeRateEachSeason, element) => {
            //divide no of runs (conceded/no of delievery)*100 to get strike rate for each bastman per year
            element[1] = Object.entries(element[1]);

            let strikeRateBatsman = element[1].reduce((strikeRateBatsman, currentElement) => {
                strikeRateBatsman[currentElement[0]] = ((currentElement[1][0] / currentElement[1][1]) * 100).toFixed(2);
                return strikeRateBatsman;
            }, {})

            BatsmanStrikeRateEachSeason[element[0]] = strikeRateBatsman;

            return BatsmanStrikeRateEachSeason;
        }, {})



        let jsonString = JSON.stringify(strikeRateBatsmanEachSeason, null, 2);
        fs.writeFileSync("../public/output/strikeRateOfBatsmanEachSeason.json", jsonString);
        console.log(jsonString);

    }

    StrikeRateBatsmanEachSeason();
})
