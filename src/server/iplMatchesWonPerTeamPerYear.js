const JsonDataPromise = require("./index.js");
const fs = require('fs');

JsonDataPromise.JSONMatchesDataFetcher().then((data) => {


    function MatchesWonPerTeamPerYear() {
        let matchesWonPerTeamPerYear = {};

        let answer = data.reduce((matchesWonPerTeamPerYear, MatchObject) => {
            if (MatchObject.winner == "")
                return matchesWonPerTeamPerYear;

            if (MatchObject.winner == "Rising Pune Supergiants")
                MatchObject.winner = "Rising Pune Supergiant";

            if (matchesWonPerTeamPerYear[MatchObject.winner] == undefined) {
                matchesWonPerTeamPerYear[MatchObject.winner] = {};
                matchesWonPerTeamPerYear[MatchObject.winner][MatchObject.season] = 1;
            }
            else {
                if (matchesWonPerTeamPerYear[MatchObject.winner][MatchObject.season] == undefined)
                    matchesWonPerTeamPerYear[MatchObject.winner][MatchObject.season] = 1;
                else
                    matchesWonPerTeamPerYear[MatchObject.winner][MatchObject.season] += 1;
            }

                return matchesWonPerTeamPerYear;
        }, {})



        let jsonString = JSON.stringify(answer, null, 2);
        fs.writeFileSync("../public/output/matchesWonPerTeamPerYear.json", jsonString);
        console.log(jsonString);
      
    }

    MatchesWonPerTeamPerYear();



})